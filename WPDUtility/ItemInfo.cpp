#include "stdafx.h"
#include "ItemInfo.h"

using namespace System::Xml;
using namespace System::Xml::Serialization;

////////////////////////////////
ItemFile::ItemFile()
{
}

ItemFile::~ItemFile()
{
}


/////////////////////////////////
Folder::Folder()
{
	mListFile = gcnew List<ItemFile^>();
	mListFolder = gcnew List<Folder^>();
}

Folder::~Folder()
{
}

////////////////////////////

CItemInfo::CItemInfo()
{
	mListFile = gcnew List<ItemFile^>();
	mListFolder = gcnew List<Folder^>();
}

CItemInfo::CItemInfo(String^ s)
{
	mListFile = gcnew List<ItemFile^>();
	mListFolder = gcnew List<Folder^>();
	sfolder = s;
}


void CItemInfo::XmlSerializeInternal(Stream^ stream, Object^ o, Encoding^ encoding)
{
	if (o == nullptr)
		throw gcnew ArgumentNullException("o");
	if (encoding == nullptr)
		throw gcnew ArgumentNullException("encoding");

	XmlSerializer^ serializer = gcnew XmlSerializer(o->GetType());

	XmlWriterSettings^ settings = gcnew XmlWriterSettings();
	settings->Indent = true;
	settings->NewLineChars = "\r\n";
	settings->Encoding = encoding;
	settings->IndentChars = "    ";

	// 不生成声明头
	settings->OmitXmlDeclaration = true;

	// 强制指定命名空间，覆盖默认的命名空间。
	XmlSerializerNamespaces^ namespaces = gcnew XmlSerializerNamespaces();
	namespaces->Add(String::Empty, String::Empty);

	if (stream == nullptr)
	{
		stream = Console::OpenStandardOutput();
	}

	String^ sHe = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<!--this is desinged by Jefferyzhang-->\r\n";
	stream->Write(System::Text::UTF8Encoding::UTF8->GetBytes(sHe), 0, sHe->Length);
	XmlWriter^ writer = XmlWriter::Create(stream, settings);
	
	serializer->Serialize(writer, o, namespaces);
	writer->Close();
	
}