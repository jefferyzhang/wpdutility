#include "stdafx.h"
#include "cHcommon.h"
#include <Objbase.h>
#include <PortableDeviceApi.h>
#include <PortableDevice.h>
#include <atlstr.h>

#include <map>

#pragma comment(lib, "PortableDeviceGuids.lib")
typedef struct {
	wchar_t *name;
	unsigned int major_version;
	unsigned int minor_version;
	unsigned int revision;
} ClientInfo;


	IPortableDeviceManager *portable_device_manager = NULL;
	static int _com_initialized = 0;
	static BOOL _bRpcChangeMode = false;
	ClientInfo client_info = { NULL, 1, 0, 0 };

#define ADDPROP(x) hr = properties->Add(x); if (FAILED(hr)) { logIt(_T("Failed to add property to filesystem properties collection = 0x%x\n", hr); properties->Release(); return NULL; }
#define ENSURE_WPD(retval) \
    if (portable_device_manager == NULL) { logIt(_T("No WPD service available.\n")); return retval; }


namespace WPD{
	HRESULT wpd_init(ClientInfo client_info) { //_tcsdup
		HRESULT hr;
		
		if (!_com_initialized)  
		{
			hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
			if (SUCCEEDED(hr) || hr == RPC_E_CHANGED_MODE)
			{
				_com_initialized = 1;
				_bRpcChangeMode = hr == RPC_E_CHANGED_MODE;
			}
			else 
			{ 
				logIt(_T("Failed to initialize COM")); 
				return NULL; 
			}
		}

		if (portable_device_manager == NULL) {
			hr = CoCreateInstance(CLSID_PortableDeviceManager, NULL,
				CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&portable_device_manager));

			if (FAILED(hr)) {
				portable_device_manager = NULL;
				return NULL;
			}
		}
		return hr;
	}

	static void wpd_uninit() {
		if (portable_device_manager != NULL) {
			portable_device_manager->Release();
			portable_device_manager = NULL;
		}

		if (_com_initialized && !_bRpcChangeMode) {
			CoUninitialize();
		}
		_com_initialized = 0;

		if (client_info.name != NULL) { free(client_info.name); }		
	}

	IPortableDeviceValues *get_client_information() { // {{{
		IPortableDeviceValues *client_information;
		HRESULT hr;

		ENSURE_WPD(NULL);

		hr = CoCreateInstance(CLSID_PortableDeviceValues, NULL,
			CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&client_information));
		if (FAILED(hr)) { logIt(_T("Failed to create IPortableDeviceValues = 0x%x\n"), hr); return NULL; }

		hr = client_information->SetStringValue(WPD_CLIENT_NAME, client_info.name);
		if (FAILED(hr)) { logIt(_T("Failed to set client name = 0x%x\n"), hr); return NULL; }
		hr = client_information->SetUnsignedIntegerValue(WPD_CLIENT_MAJOR_VERSION, client_info.major_version);
		if (FAILED(hr)) { logIt(_T("Failed to set major version = 0x%x\n"), hr); return NULL; }
		hr = client_information->SetUnsignedIntegerValue(WPD_CLIENT_MINOR_VERSION, client_info.minor_version);
		if (FAILED(hr)) { logIt(_T("Failed to set minor version = 0x%x\n"), hr); return NULL; }
		hr = client_information->SetUnsignedIntegerValue(WPD_CLIENT_REVISION, client_info.revision);
		if (FAILED(hr)) { logIt(_T("Failed to set revision = 0x%x\n"), hr); return NULL; }
		//  Some device drivers need to impersonate the caller in order to function correctly.  Since our application does not
		//  need to restrict its identity, specify SECURITY_IMPERSONATION so that we work with all devices.
		hr = client_information->SetUnsignedIntegerValue(WPD_CLIENT_SECURITY_QUALITY_OF_SERVICE, SECURITY_IMPERSONATION);
		if (FAILED(hr)) { logIt(_T("Failed to set quality of service = 0x%x\n"), hr); return NULL; }
		return client_information;
	} // }}}


	IPortableDevice *open_device(const wchar_t *pnp_id, IPortableDeviceValues *client_information) { // {{{
		IPortableDevice *device = NULL;
		HRESULT hr;

		hr = CoCreateInstance(CLSID_PortableDevice, NULL, CLSCTX_INPROC_SERVER,
			IID_PPV_ARGS(&device));
		if (FAILED(hr)) logIt(_T("Failed to create IPortableDevice %d\n"), hr);
		else {
			hr = device->Open(pnp_id, client_information);
			if FAILED(hr) {
				device->Release();
				device = NULL;
				if (hr == E_ACCESSDENIED)
					logIt(_T("Read/write access to device is denied\n"));
				else
					logIt(_T("Failed to open device =%d\n"), hr);
			}
		}

		return device;

	} // }}}

	std::map<CString, CString> get_storage_info(IPortableDevice *device) { // {{{
		HRESULT hr, hr2;
		IPortableDeviceContent *content = NULL;
		IEnumPortableDeviceObjectIDs *objects = NULL;
		IPortableDeviceProperties *properties = NULL;
		IPortableDeviceKeyCollection *storage_properties = NULL;
		IPortableDeviceValues *values = NULL;
		DWORD fetched, i;
		PWSTR object_ids[10];
		GUID guid;
		ULONGLONG capacity, free_space, capacity_objects, free_objects;
		ULONG access, storage_type = WPD_STORAGE_TYPE_UNDEFINED;
		LPWSTR storage_desc = NULL, st = NULL;
		std::map<CString, CString> ret;

		hr = device->Content(&content);
		if (FAILED(hr)) { logIt(_T("Failed to get content interface from device  = 0x%x\n"), hr); goto end; }

		hr = content->Properties(&properties);
		if (FAILED(hr)) { logIt(_T("Failed to get properties interface = 0x%x\n"), hr); goto end; }

		hr = CoCreateInstance(CLSID_PortableDeviceKeyCollection, NULL,
			CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&storage_properties));
		if (FAILED(hr)) { logIt(_T("Failed to create storage properties collection = 0x%x\n"), hr); goto end; }

		hr = storage_properties->Add(WPD_OBJECT_CONTENT_TYPE);
		hr = storage_properties->Add(WPD_FUNCTIONAL_OBJECT_CATEGORY);
		hr = storage_properties->Add(WPD_STORAGE_DESCRIPTION);
		hr = storage_properties->Add(WPD_STORAGE_CAPACITY);
		hr = storage_properties->Add(WPD_STORAGE_CAPACITY_IN_OBJECTS);
		hr = storage_properties->Add(WPD_STORAGE_FREE_SPACE_IN_BYTES);
		hr = storage_properties->Add(WPD_STORAGE_FREE_SPACE_IN_OBJECTS);
		hr = storage_properties->Add(WPD_STORAGE_ACCESS_CAPABILITY);
		hr = storage_properties->Add(WPD_STORAGE_FILE_SYSTEM_TYPE);
		hr = storage_properties->Add(WPD_STORAGE_TYPE);
		hr = storage_properties->Add(WPD_OBJECT_NAME);
		hr = storage_properties->Add(WPD_STORAGE_SERIAL_NUMBER);
		if (FAILED(hr)) { logIt(_T("Failed to create collection of properties for storage query = 0x%x\n"), hr); goto end; }

		hr = content->EnumObjects(0, WPD_DEVICE_OBJECT_ID, NULL, &objects);
		if (FAILED(hr)) { logIt(_T("Failed to get objects from device-0x%x\n"), hr); goto end; }

		hr = S_OK;
		while (hr == S_OK) {
			hr = objects->Next(10, object_ids, &fetched);
			if (SUCCEEDED(hr)) {
				for (i = 0; i < fetched; i++) {
					hr2 = properties->GetValues(object_ids[i], storage_properties, &values);
					if SUCCEEDED(hr2) {
						if (
							SUCCEEDED(values->GetGuidValue(WPD_OBJECT_CONTENT_TYPE, &guid)) && IsEqualGUID(guid, WPD_CONTENT_TYPE_FUNCTIONAL_OBJECT) &&
							SUCCEEDED(values->GetGuidValue(WPD_FUNCTIONAL_OBJECT_CATEGORY, &guid)) && IsEqualGUID(guid, WPD_FUNCTIONAL_CATEGORY_STORAGE)
							) {
							capacity = 0; capacity_objects = 0; free_space = 0; free_objects = 0;
							CString ss;
							values->GetUnsignedLargeIntegerValue(WPD_STORAGE_CAPACITY, &capacity);
							ss.Format(_T("%lld"), capacity);
							ret.insert(std::pair<CString, CString>(_T("WPD_STORAGE_CAPACITY"), ss));
							values->GetUnsignedLargeIntegerValue(WPD_STORAGE_CAPACITY_IN_OBJECTS, &capacity_objects);
							//ss.Format(_T("%lld"), capacity_objects);
							//ret.insert(std::pair<CString, CString>(_T("WPD_STORAGE_CAPACITY_IN_OBJECTS"), ss));
							values->GetUnsignedLargeIntegerValue(WPD_STORAGE_FREE_SPACE_IN_BYTES, &free_space);
							ss.Format(_T("%lld"), free_space);
							ret.insert(std::pair<CString, CString>(_T("WPD_STORAGE_FREE_SPACE_IN_BYTES"), ss));
							values->GetUnsignedLargeIntegerValue(WPD_STORAGE_FREE_SPACE_IN_OBJECTS, &free_objects);
							//ss.Format(_T("%lld"), free_objects);
							//ret.insert(std::pair<CString, CString>(_T("WPD_STORAGE_FREE_SPACE_IN_OBJECTS"), ss));
							values->GetUnsignedIntegerValue(WPD_STORAGE_TYPE, &storage_type);
							if (SUCCEEDED(values->GetUnsignedIntegerValue(WPD_STORAGE_ACCESS_CAPABILITY, &access)) && access == WPD_STORAGE_ACCESS_CAPABILITY_READWRITE){
							//	desc = Py_True;
							}
							if (SUCCEEDED(values->GetStringValue(WPD_STORAGE_DESCRIPTION, &storage_desc))) {
								ret.insert(std::pair<CString, CString>(_T("WPD_STORAGE_DESCRIPTION"), storage_desc));
								CoTaskMemFree(storage_desc); storage_desc = NULL;
							}
							if (SUCCEEDED(values->GetStringValue(WPD_OBJECT_NAME, &storage_desc))) {
								ret.insert(std::pair<CString, CString>(_T("WPD_OBJECT_NAME"), storage_desc));
								CoTaskMemFree(storage_desc); storage_desc = NULL;
							}
							if (SUCCEEDED(values->GetStringValue(WPD_STORAGE_FILE_SYSTEM_TYPE, &storage_desc))) {
								CoTaskMemFree(storage_desc); storage_desc = NULL;
							}
							if (SUCCEEDED(values->GetStringValue(WPD_STORAGE_SERIAL_NUMBER, &storage_desc))) {
								ret.insert(std::pair<CString, CString>(_T("WPD_STORAGE_SERIAL_NUMBER"), storage_desc));
								CoTaskMemFree(storage_desc); storage_desc = NULL;
							}
							switch (storage_type) {
							case WPD_STORAGE_TYPE_REMOVABLE_RAM:
								st = L"removable_ram";
								break;
							case WPD_STORAGE_TYPE_REMOVABLE_ROM:
								st = L"removable_rom";
								break;
							case WPD_STORAGE_TYPE_FIXED_RAM:
								st = L"fixed_ram";
								break;
							case WPD_STORAGE_TYPE_FIXED_ROM:
								st = L"fixed_rom";
								break;
							default:
								st = L"unknown_unknown";
							}
							ret.insert(std::pair<CString, CString>(_T("WPD_STORAGE_TYPE"), st));
						}
					}
				}
				for (i = 0; i < fetched; i++) { CoTaskMemFree(object_ids[i]); object_ids[i] = NULL; }
			}// if(SUCCEEDED(hr))
		}

	end:
		if (content != NULL) content->Release();
		if (objects != NULL) objects->Release();
		if (properties != NULL) properties->Release();
		if (storage_properties != NULL) storage_properties->Release();
		if (values != NULL) values->Release();

		return ret;
	} // }}}

}


void getStorageInfo(TCHAR *id)
{
	ENTER_FUNCTION();
	if (id == NULL || _tcslen(id) == 0) return;

	client_info.name = _tcsdup(id);
	if (SUCCEEDED(WPD::wpd_init(client_info)))
	{
		IPortableDevice *pdev = WPD::open_device(id, WPD::get_client_information());
		if (pdev != NULL)
		{
			std::map<CString, CString> m = WPD::get_storage_info(pdev);
			std::map<CString, CString>::iterator it;
			for (it = m.begin(); it != m.end();it++)
			{
				_tprintf(_T("%s=%s\n"), it->first, it->second);
			}
		}
		WPD::wpd_uninit();
	}


}