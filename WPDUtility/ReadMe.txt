		"All regex is used. So aaa.jpg ismatch aaaa.jpg. MUST ^aaa.jpg$\n"
		" -enum                         - List all WPD devices \n"
		" -label=X                      - for log label.\n"
		"\n"
		" -hubname=<hubname>            - connected usb hub device\n"
		" -hubport=X                    - connected usb hub port\n"
		" -id=<device name>             - WPD symbolink. if this exist, ignore hub info.\n"
		"\n"
		" -pcfn=<pc filename>           - pc file name\n"
		" -devfn=<dev file name>        - file name in device \n"
		"\n"
		"commands:\n"
		"  push <devfn> <pcfn>			- copy file/dir to device\n"
		"  pull <devfn> <pcfn>			- copy file/dir from device\n"
		"  ls= <device directory>		- list device folder files to xml.\n"
		"  mkdir <devfn>                - device create folder.\n"
		"  delete <devfn>               - delete file or folder in device\n"
		"  exist  <devfn>               - file in device is exist or not\n"


		read info add some information:
findid=\\?\usb#vid_04e8&pid_6860&ms_comp_mtp&samsung_android#6&32e6dbe3&0&0000#{6ac27878-a6fa-4155-ba85-f98f491d4f33}
Battery=100
Content=
DeviceId=\\?\usb#vid_04e8&pid_6860&ms_comp_mtp&samsung_android#6&32e6dbe3&0&0000#{6ac27878-a6fa-4155-ba85-f98f491d4f33}
WPD_OBJECT_NAME=Phone
WPD_STORAGE_CAPACITY=25057132544
WPD_STORAGE_DESCRIPTION=Phone
WPD_STORAGE_FREE_SPACE_IN_BYTES=24200216576
WPD_STORAGE_SERIAL_NUMBER=SECZ9519043CHOHB
WPD_STORAGE_TYPE=fixed_ram
DeviceType=2
FirmwareVersion=N915VVRU2BOG5
FriendlyName=SAMSUNG-SM-N915V
Model=SM-N915V
SerialNumber=58dcc6a9