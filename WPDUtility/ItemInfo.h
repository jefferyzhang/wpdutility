#pragma once

using namespace System;
using namespace System::Collections::Generic;
using namespace System::IO;
using namespace System::Text;
using namespace System::Xml::Serialization;

public ref class ItemFile
{
public:
	ItemFile();
	~ItemFile();

	[XmlAttribute]
	String^ objectID;
	[XmlAttribute]
	String^ type;
	[XmlAttribute]
	String^ FileName;
	[XmlAttribute]
	String^ Name;
};


public ref class Folder
{
public:
	Folder();
	~Folder();

	[XmlAttribute]
	String^ objectID;
	[XmlAttribute]
	String^ type;
	[XmlAttribute]
	String^ Name;

	[XmlElement("file")]
	List<ItemFile^>^ mListFile;
	[XmlElement("folder")]
	List<Folder^>^ mListFolder;
private:

};

[XmlType("root")]
public ref class CItemInfo
{
public:
	CItemInfo();
	CItemInfo(String^ s);
	[XmlAttribute("name")]
	String^ sfolder;

	[XmlElement("file")]
	List<ItemFile^>^ mListFile;
	[XmlElement("folder")]
	List<Folder^>^ mListFolder;

	static void XmlSerializeInternal(Stream^ stream, Object^ o, Encoding^ encoding);
};

