#pragma once 
#include <Windows.h>
#include <tchar.h>

int GetWpdDeviceId(TCHAR* hubName, int hubport, TCHAR* ids, int nIDLen);
VOID
Oops(
	__in PCTSTR File,
	ULONG Line,
	DWORD dwError);

void logIt(TCHAR* fmt, ...);