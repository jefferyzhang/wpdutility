// WPDUtility.cpp : main project file.

#include "stdafx.h"
#include <stdio.h>
#include "cHcommon.h"
#include "ItemInfo.h"

#include <vcclr.h>

using namespace System;
using namespace System::Collections::Generic;
using namespace PortableDeviceLib;
using namespace PortableDeviceLib::Model;
using namespace System::Collections::Generic;
using namespace System::Collections::ObjectModel;

#define APPNAME		L"PortableDeviceTools"
#define APPMAJORVERSIONNUMBER	1
#define APPMINORVERSIONNUMBER	0

void logIt(String^ s, bool consoleout = FALSE)
{
	extern int nLabel;
	System::Diagnostics::Trace::WriteLine(String::Format("[Label_{0}]: {1}", nLabel, s));
	if (consoleout)
	{
		Console::WriteLine(s);
	}
}

void PrintDevice(PortableDevice^ device)
{
	logIt("Battery:" + device->BatteryLevel);
	logIt("Content:" + device->Content);
	//Console::Write(";DeviceCapabilities:" + device->DeviceCapabilities);
	logIt("DeviceId:" + device->DeviceId);
	logIt("DeviceType:" + device->DeviceType);
	logIt("FirmwareVersion:" + device->FirmwareVersion);
	logIt("FriendlyName:" + device->FriendlyName);
	logIt("Model:" + device->Model);
	logIt("SerialNumber:" + device->SerialNumber);
}

void FindAllObject(PortableDeviceObject^ a, StorageServicesAdapter^ ssa, Folder^ folder)
{
	if (a->ContentType == "WPD_CONTENT_TYPE_FOLDER")
	{
		Folder^ temp = gcnew Folder();
		folder->mListFolder->Add(temp);
		temp->Name = a->Name;
		temp->objectID = a->ID;
		logIt(a->Name);

		for each (PortableDeviceObject^ aa in ssa->Find(".*", (PortableDeviceContainerObject^)a))
		{

			FindAllObject(aa, ssa, temp);
		}
	}
	else if (String::IsNullOrEmpty(a->ContentType) || a->ContentType == "WPD_CONTENT_TYPE_FUNCTIONAL_OBJECT")
	{
		Folder^ temp = gcnew Folder();
		folder->mListFolder->Add(temp);
		temp->Name = a->Name;
		temp->objectID = a->ID;
		for each (PortableDeviceObject^ aa in ssa->Find(".*", (PortableDeviceContainerObject^)a))
		{

			FindAllObject(aa, ssa, temp);
		}
	}
	else
	{
		logIt(a->ContentType);
		ItemFile^ file = gcnew ItemFile();
		file->FileName = ((PortableDeviceLib::Model::PortableDeviceFileObject^) a)->FileName;
		file->Name = a->ID;
		file->objectID = a->ID;
		folder->mListFile->Add(file);
		logIt(a->ID + ":" + a->Name + ":" + ((PortableDeviceLib::Model::PortableDeviceFileObject^) a)->FileName);
	}
}

void Usages()
{
	fprintf(stderr,
		"All regex is used. So aaa.jpg ismatch aaaa.jpg. MUST ^aaa.jpg$\n"
		" -enum                         - List all WPD devices \n"
		" -label=X                      - for log label.\n"
		"\n"
		" -hubname=<hubname>            - connected usb hub device\n"
		" -hubport=X                    - connected usb hub port\n"
		" -id=<device name>             - WPD symbolink. if this exist, ignore hub info.\n"
		"\n"
		" -pcfn=<pc filename>           - pc file name\n"
		" -devfn=<dev file name>        - file name in device \n"
		"\n"
		"commands:\n"
		"  push <devfn> <pcfn>			- copy file/dir to device\n"
		"  pull <devfn> <pcfn>			- copy file/dir from device\n"
		"  ls= <device directory>		- list device folder files to xml.\n"
		"  mkdir <devfn>                - device create folder.\n"
		"  delete <devfn>               - delete file or folder in device\n"
		"  exist  <devfn>               - file in device is exist or not\n"
		);
}

int main(array<System::String ^> ^args)
{
	int ret = ERROR_SUCCESS;
	System::Configuration::Install::InstallContext^ _param = gcnew System::Configuration::Install::InstallContext(nullptr, args);

	if (_param->IsParameterTrue("debug"))
	{
		Console::WriteLine("wait for debug. press any key to contiune.");
		Console::ReadKey();
	}
	if (_param->Parameters->ContainsKey("label"))
	{
		extern int nLabel;
		nLabel = Convert::ToInt32(_param->Parameters["label"]);
	}
	if (_param->IsParameterTrue("help") || _param->IsParameterTrue("h"))
	{
		Usages();
		return ret;
	}
	String^ sCurId="\\\\?\\usb#vid_22b8&pid_2e76&mi_00#6&2bec1e42&1&0000#{6ac27878-a6fa-4155-ba85-f98f491d4f33}";

	if (_param->Parameters->ContainsKey("hubname") && _param->Parameters->ContainsKey("hubport"))
	{
		TCHAR sID[1024] = { 0 };
		int nLen = 1024;
		pin_ptr<const wchar_t> convertedValue = PtrToStringChars(_param->Parameters["hubname"]);
		GetWpdDeviceId((TCHAR *)convertedValue, Convert::ToInt32(_param->Parameters["hubport"]), sID, nLen);
		sCurId = gcnew String(sID);
	}

	if (_param->Parameters->ContainsKey("id"))
	{
		sCurId = _param->Parameters["id"];
	}

	String^ sPcFile;
	String^ sDevFile;
	if (_param->Parameters->ContainsKey("pcfn"))
	{
		sPcFile = _param->Parameters["pcfn"];
	}
	if (_param->Parameters->ContainsKey("devfn"))
	{
		sDevFile = _param->Parameters["devfn"];
	}


	PortableDevice^ selectedPortableDevice;

	if (PortableDeviceLib::PortableDeviceCollection::Instance == nullptr)
	{
		PortableDeviceCollection::CreateInstance(APPNAME, APPMAJORVERSIONNUMBER, APPMINORVERSIONNUMBER);
		PortableDeviceCollection::Instance->AutoConnectToPortableDevice = false;
	}

	for each(PortableDevice^ device in PortableDeviceCollection::Instance->Devices)
	{
		if (_param->IsParameterTrue("enum"))
			logIt(device->ToString(), true);
		if (String::Compare(device->DeviceId, sCurId, true) == 0)
		{
			selectedPortableDevice = device;
			if (!_param->IsParameterTrue("enum"))
				break;
		}
	}
	if (selectedPortableDevice == nullptr)
	{
		if (!_param->IsParameterTrue("enum"))
		{
			Usages();
			return ERROR_INVALID_SHOWWIN_COMMAND;
		}
		return ERROR_SUCCESS;
	}
	selectedPortableDevice->ConnectToDevice(APPNAME, APPMAJORVERSIONNUMBER, APPMINORVERSIONNUMBER);
	PrintDevice(selectedPortableDevice);
	if (_param->IsParameterTrue("info"))
	{
		if (selectedPortableDevice != nullptr)
		{
			Console::WriteLine("Battery=" + selectedPortableDevice->BatteryLevel);
			Console::WriteLine("Content=" + selectedPortableDevice->Content);
			//StorageDevice

#undef DeviceCapabilities
			//			Console::WriteLine("DeviceCapabilities=" + selectedPortableDevice->DeviceCapabilities->ToString());
			Console::WriteLine("DeviceId=" + selectedPortableDevice->DeviceId);
			TCHAR sID[1024] = { 0 };
			int nLen = 1024;
			pin_ptr<const wchar_t> convertedValue = PtrToStringChars(selectedPortableDevice->DeviceId);
			getStorageInfo((TCHAR *)convertedValue);

			Console::WriteLine("DeviceType=" + selectedPortableDevice->DeviceType);
			Console::WriteLine("FirmwareVersion=" + selectedPortableDevice->FirmwareVersion);
			Console::WriteLine("FriendlyName=" + selectedPortableDevice->FriendlyName);
			Console::WriteLine("Model=" + selectedPortableDevice->Model);
			Console::WriteLine("SerialNumber=" + selectedPortableDevice->SerialNumber);
			//Console::WriteLine("StorageCapacity=" + selectedPortableDevice->StorageCapacity);

		}
		return ERROR_SUCCESS;
	}


	AllObjectEnumerateHelper^ Helper = gcnew AllObjectEnumerateHelper();
	selectedPortableDevice->RefreshContent(Helper);

	
	//String^ sasss = selectedPortableDevice->Content->Name;
	//for each (PortableDeviceObject^ a in selectedPortableDevice->Content->Childs)
	//{
	//	Console::WriteLine(a->ToString());
	//	Console::WriteLine(a->ContentType);
	//	Console::WriteLine(a->ID);
	//	Console::WriteLine(a->Format);
	//}

	//Helper->Next
	StorageServicesAdapter^ ssa = gcnew StorageServicesAdapter(selectedPortableDevice);
	logIt("Storages count:"+ssa->Storages->Count);
	
	
	//FindAllObject(selectedPortableDevice->Content, ssa);
	logIt("///////////////////////////////////////////");
	if (_param->Parameters->ContainsKey("ls"))
	{
		String^ sRoot = _param->Parameters["ls"];
		CItemInfo^ itinfo = gcnew CItemInfo(sRoot);

		if (sRoot == "/") sRoot = "/.*";

		for each (PortableDeviceObject^ a in ssa->Find(sRoot))
		{
			if (a->ContentType == "WPD_CONTENT_TYPE_FOLDER")
			{
				Folder^ folder = gcnew Folder();
				itinfo->mListFolder->Add(folder);
				folder->Name = a->Name;
				folder->objectID = a->ID;
				logIt(a->Name);
				for each (PortableDeviceObject^ aa in ssa->Find(".*", (PortableDeviceContainerObject^)a))
				{
					FindAllObject(aa, ssa, folder);
				}
			}
			else if (String::IsNullOrEmpty(a->ContentType) || a->ContentType == "WPD_CONTENT_TYPE_FUNCTIONAL_OBJECT")
			{
				Folder^ folder = gcnew Folder();
				itinfo->mListFolder->Add(folder);
				folder->Name = a->Name;
				folder->objectID = a->ID;
				for each (PortableDeviceObject^ aa in ssa->Find(".*", (PortableDeviceContainerObject^)a))
				{
					FindAllObject(aa, ssa, folder);
				}
			}
			else
			{
				logIt(a->ContentType);
				ItemFile^ file = gcnew ItemFile();
				file->FileName = ((PortableDeviceLib::Model::PortableDeviceFileObject^) a)->FileName;
				file->Name = a->ID;
				file->objectID = a->ID;
				itinfo->mListFile->Add(file);
				logIt(a->ID + ":" + a->Name + ":" + ((PortableDeviceLib::Model::PortableDeviceFileObject^) a)->FileName);
			}

			//FindAllObject(a, ssa);
			/*
			Console::WriteLine(a->ToString());
			Console::WriteLine(a->Format);
			Console::WriteLine(a->ContentType);
			System::IO::FileStream^ sourceStream = gcnew System::IO::FileStream("J:\\temp\\aaa.txt", System::IO::FileMode::Open, System::IO::FileAccess::Read);
			System::IO::FileInfo^ finfo = gcnew System::IO::FileInfo("J:\\temp\\aaa.txt");
			ssa->Push((PortableDeviceContainerObject^)a, sourceStream, finfo->Name, finfo->Length);
			sourceStream->Close();
			if (ssa->Exists("/Ringtones/aaa.txt"))
			{
			ssa->Delete(a, StorageServicesAdapter::DeleteObjectOptions::WITH_RECURSION);
			}
			*/
		}

		FileStream^ fs = nullptr;
		if (!String::IsNullOrEmpty(sPcFile))
		{
			fs = gcnew FileStream(sPcFile, System::IO::FileMode::OpenOrCreate);
		}

		CItemInfo::XmlSerializeInternal(fs, itinfo, UTF8Encoding::UTF8);
		if (fs!=nullptr)
			fs->Close();

		ret = ERROR_SUCCESS;
	}
	if (_param->Parameters->ContainsKey("pull"))
	{
		if (String::IsNullOrEmpty(sDevFile) || String::IsNullOrEmpty(sPcFile))
		{
			Usages();
			ret = ERROR_INVALID_SHOWWIN_COMMAND;
			return ret;
		}
		for each (PortableDeviceObject^ a in ssa->Find(sDevFile))
		{
			System::IO::FileStream^ sourceStream = gcnew System::IO::FileStream(sPcFile, System::IO::FileMode::OpenOrCreate, System::IO::FileAccess::Write);			
			ssa->Pull((PortableDeviceContainerObject^)a, sourceStream);
			sourceStream->Close();
			ret = ERROR_SUCCESS;
			break;
		}
	}
	if (_param->Parameters->ContainsKey("push"))
	{
		if (String::IsNullOrEmpty(sDevFile) || String::IsNullOrEmpty(sPcFile) || !File::Exists(sPcFile))
		{
			Usages();
			ret = ERROR_INVALID_SHOWWIN_COMMAND;
			return ret;
		}
		String^ sRoot = sDevFile;
		
		for each (PortableDeviceObject^ a in ssa->Find(sRoot))
		{
			System::IO::FileStream^ sourceStream = gcnew System::IO::FileStream(sPcFile, System::IO::FileMode::Open, System::IO::FileAccess::Read);
			System::IO::FileInfo^ finfo = gcnew System::IO::FileInfo(sPcFile);
			ssa->Push((PortableDeviceContainerObject^)a, sourceStream, finfo->Name, finfo->Length);
			sourceStream->Close();
			ret = ERROR_SUCCESS;
			break;
		}
	}
	if (_param->Parameters->ContainsKey("delete"))
	{
		if (String::IsNullOrEmpty(sDevFile))
		{
			Usages();
			ret = ERROR_INVALID_SHOWWIN_COMMAND;
			return ret;
		}
		for each (PortableDeviceObject^ a in ssa->Find(sDevFile))
		{
			ssa->Delete(a, StorageServicesAdapter::DeleteObjectOptions::WITH_RECURSION);	
		}
	}
	if (_param->Parameters->ContainsKey("exist"))
	{
		if (String::IsNullOrEmpty(sDevFile))
		{
			Usages();
			ret = ERROR_INVALID_SHOWWIN_COMMAND;
			return ret;
		}
		if (ssa->Exists(sDevFile))
			return ERROR_SUCCESS;
		else
			return ERROR_FILE_NOT_FOUND;
	}
	if (_param->Parameters->ContainsKey("mkdir"))
	{
		if (String::IsNullOrEmpty(sDevFile))
		{
			Usages();
			ret = ERROR_INVALID_SHOWWIN_COMMAND;
			return ret;
		}
		String^ sRoot = Path::GetDirectoryName(sDevFile);
		String^ sName = System::IO::Path::GetFileName(sDevFile);
		for each (PortableDeviceObject^ a in ssa->Find(sRoot))
		{
			ssa->Mkdir((PortableDeviceContainerObject^)a, sName);
			ret = ERROR_SUCCESS;
		}
	}

	return ret;
}
